﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Unit5_DataBinding
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class History : ContentPage
    {
        public History()
        {
            this.BindingContext = new HistoryViewModel();
            InitializeComponent();
        }

        private async void list_ItemTapped(object sender, ItemTappedEventArgs e)
        {
         await    this.Navigation.PushAsync(new ProductDetail(e.Item as ProductViewModel));
        }
    }
}
