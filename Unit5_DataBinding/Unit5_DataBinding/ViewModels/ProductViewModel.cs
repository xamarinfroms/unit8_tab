﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Unit5.Core;

namespace Unit5_DataBinding
{
    public class ProductViewModel: INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public ProductViewModel(Product data)
        {
            this._data = data;
        }

        public string Name
        {
            get { return this._data.Name; }
            set { this._data.Name = value; this.FireUpdate(nameof(this.Name)); }
        }

        public string ImageName
        {
            get { return this._data.ImageName; }
        }

        public string Desc
        {
            get { return this._data.Desc; }
        }
        public int Price
        {
            get { return this._data.Price; }
        }

        public int Count
        {
            get { return this._data.Count; }

        }
        public bool IsOnSale
        {
            get { return this._data.IsOnSale; }

        }

        public void FireUpdate(string name)
        {
            this.PropertyChanged?.DynamicInvoke(this, new PropertyChangedEventArgs(name));
        }

        private Product _data;
    }
}
