﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Unit5.Core;

namespace Unit5_DataBinding
{
    public class ProductDetailViewModel
    {
        public Product ProductData { get; set; }
        public ProductDetailViewModel(Product productData)
        {
            this.ProductData = productData;
        }

        public string Name
        {
            get { return this.ProductData.Name; }
        }

        public string ImageName
        {
            get { return this.ProductData.ImageName; }
        }

        public string Desc
        {
            get { return this.ProductData.Desc; }
        }

        public int Price
        {
            get { return this.ProductData.Price; }
        }
    }
}
